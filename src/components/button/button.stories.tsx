import React from "react";
import { action } from "@storybook/addon-actions";
import { withKnobs, text, boolean, select } from "@storybook/addon-knobs";
import Button from "./index";

export default {
  title: "Button",
  component: Button,
  decorators: [withKnobs]
};

export const Default = () => (
  <>
    <Button onClick={action("clicked")}>default</Button>
    <Button appearance="primary" onClick={action("clicked")}>
      Primary
    </Button>
    <Button appearance="dashed" onClick={action("clicked")}>
      Dashed
    </Button>
  </>
);

export const Size = () => (
  <>
    <Button size="small" onClick={action("clicked")}>
      Small
    </Button>
    <Button size="middle" appearance="primary" onClick={action("clicked")}>
      Middle
    </Button>
    <Button size="large" appearance="dashed" onClick={action("clicked")}>
      Large
    </Button>
  </>
);

export const Loading = () => (
  <>
    <Button
      appearance='default'
      loading={boolean("loading", false)}
      onClick={action("clicked")}
    >
      Small
    </Button>
    <Button
      loading={boolean("loading", false)}
      appearance="primary"
      onClick={action("clicked")}
    >
      Middle
    </Button>
    <Button
      loading={boolean("loading", false)}
      appearance="dashed"
      onClick={action("clicked")}
    >
      Large
    </Button>
  </>
);

export const Disabled = () => (
  <>
    <Button
      disabled={boolean("disabled", false)}
      loading={boolean("loading", false)}
      onClick={action("clicked")}
    >
      Small
    </Button>
    <Button
      disabled={boolean("disabled", false)}
      loading={boolean("loading", false)}
      appearance="primary"
      onClick={action("clicked")}
    >
      Middle
    </Button>
    <Button
      disabled={boolean("disabled", false)}
      loading={boolean("loading", false)}
      appearance="dashed"
      size='large'
      onClick={action("clicked")}
    >
      Large
    </Button>
  </>
);
