import React from "react";
import { render, cleanup } from "@testing-library/react";
import { ButtonProps, btnPaddingObj } from "../button";
import Button from '../index';
import { color, typography } from "../../shared/styles";
import {GlobalStyle} from '../../shared/global';

const defaultProps = {
	onClick: jest.fn(),
	className: "testprops",
};
const testProps: ButtonProps = {
	appearance: "primary",
	size: "small",
	className: "testprops",
};
const disabledProps: ButtonProps = {
	disabled: true,
	onClick: jest.fn(),
};

const loadingProps: ButtonProps = {
	loading: true,
	onClick: jest.fn()
}

describe("test Button Component",() => {
	it("should render the correct default button", () => {
		const {getByTestId,getByText} = render(<Button data-testid='test-btn' appearance='primary' {...defaultProps}>Primary</Button>);
		const element = getByTestId('test-btn');
		expect(element).toBeInTheDocument();

		// 正确渲染文本
		const text = getByText('Primary');
		expect(element).toBeTruthy;

		// 验证组件是否正确渲染
		expect(element.tagName).toEqual('BUTTON');
		expect(element.getAttribute('class')?.split(" ").includes('testprops')).toEqual(true);

		// 验证点击事件是否触发
		element.click();
		expect(defaultProps.onClick).toHaveBeenCalled();

		expect(element).toHaveStyle(`background:${color.primary}`);
		expect(element).toHaveStyle(`padding:${btnPaddingObj.middle}`);
	});
	it("should render disabled ", () => {
		const wrapper = render(<Button data-testid='disabled-button' {...disabledProps}>hello</Button>);
		const ele = wrapper.getByTestId("disabled-button");
		expect(ele).toBeInTheDocument();
		expect(ele).toHaveStyle("cursor: not-allowed");
		ele.click();
		expect(disabledProps.onClick).not.toHaveBeenCalled();
	});
	it("should render loading ", () => {
		const wrapper = render(<Button data-testid='loading-button' {...loadingProps}>hello</Button>);
		const ele = wrapper.getByTestId("loading-button");
		expect(ele).toBeInTheDocument();
		// expect(ele).toHaveStyle("pointer-evnets: none");
		ele.click();
		expect(disabledProps.onClick).not.toHaveBeenCalled();
	});
});

// describe("test global style", () => {
// 	it("should render the correct global style", () => {
// 		const wrapper = render(<GlobalStyle></GlobalStyle>);
// 		expect(wrapper).toMatchSnapshot();
// 	});
// });
