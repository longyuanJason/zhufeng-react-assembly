import { ReactNode, AnchorHTMLAttributes, ButtonHTMLAttributes } from "react";

export type ButtonAppearance = "primary" | "default" | "dashed";
type ButtonTypeObj = {
  [key in ButtonAppearance]: ButtonAppearance;
};
export const BUTTON_APPEARANCE: ButtonTypeObj = {
  primary: "primary",
  default: "default",
  dashed: "dashed"
};

export type SizeType = "small" | "middle" | "large";
type SizeTypeObj = {
  [key in SizeType]: SizeType;
};
export const SIZE_TYPE: SizeTypeObj = {
  small: "small",
  middle: "middle",
  large: "large"
};

export interface CustormButtonProps {
  /** 是否禁用 */
  disabled?: boolean;
  /** 是否加载中 */
  loading?: boolean;
  /** 按钮大小 */
  size?: SizeType;
  /** 按钮类型 */
  appearance?: ButtonAppearance;
}

export type ButtonProps = CustormButtonProps &
  AnchorHTMLAttributes<HTMLAnchorElement> &
  ButtonHTMLAttributes<HTMLButtonElement>;

export const btnPaddingObj = {
  small: "0 7px",
  middle: "6px 15px",
  large: "8px 15px"
};
