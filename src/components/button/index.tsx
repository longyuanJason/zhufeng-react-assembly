import React from "react";
import styled from "styled-components";
import {Icon} from "../icon/index";
import {icons} from '../shared/icons';
import {
  ButtonProps,
  BUTTON_APPEARANCE,
  SIZE_TYPE,
  btnPaddingObj
} from "./button";
import { color, action, border, typography } from "../shared/styles";
import { easing } from "../shared/animation";

const Text = styled.span`
  display: inline-block;
  vertical-align: top;
`;

const StyledButton = styled.button<ButtonProps>`
  box-sizing: border-box;
  position: relative;
  padding: ${props =>
    props.size === SIZE_TYPE.small
      ? btnPaddingObj.small
      : props.size === SIZE_TYPE.middle
      ? btnPaddingObj.middle
      : btnPaddingObj.large};
  display: inline-flex;
  align-items: center;
  margin: 0;
  text-align: center;
  user-select: none;
  text-decoration: none;
  transform: translate3d(0,0,0);
  vertical-align: top;
  cursor: pointer;
  backgroundimage: none;
  background: transparent;
  white-space: nowrap;
  box-shadow: 0 2px 0 rgba(0, 0, 0, 0.015);
  border-radius: 4px;
  font-size: ${props =>
    props.size === SIZE_TYPE.small
      ? typography.size.s1
      : props.size === SIZE_TYPE.middle
      ? typography.size.m1
      : typography.size.l1};
  font-weight: ${typography.weight.regular};
  &:hover {
    color: ${color.primary}
  };
  ${Text} {
    transform: scale3d(1, 1, 1) translate3d(0, 0, 0);
    transition: transform 700ms ${easing.rubber};
    opacity: 1;
  }
  ${props =>
    props.loading &&
    `
    pointer-events: none;
    `}
  ${props =>
    props.disabled &&
    `
    cursor: not-allowed !important;
    pointer-events: none;
    background: ${action.disabled.background1};
    border-color: ${action.disabled.borderColor1};
    opacity:0.5;
  `}

  ${props =>
    props.appearance === BUTTON_APPEARANCE.default &&
    `
    border: ${border.borderStyleDefault};
    `}

  ${props =>
    props.appearance === BUTTON_APPEARANCE.primary &&
    `
    border: ${border.borderStylePrimary};
    background: ${color.primary};
    color: ${color.lightest};
    &:hover {
     background: ${action.hover.background1};
     border-color: ${border.borderStylePrimary};
     color: ${color.lightest};
    }
    `}

  ${props =>
    props.appearance === BUTTON_APPEARANCE.dashed &&
    `
    border: ${border.borderStyleDashed};
    `}
`;

function Button(props: ButtonProps) {
  const { children, loading } = props;
  console.log('loading',loading)
  const buttonInner = (
    <>
      {loading && <Icon icon='loading' spinning={true} style={{marginRight: 4, display: 'inline-flex'}}/>}
      <Text>{children}</Text>
    </>
  );
  return <StyledButton {...props}>{buttonInner}</StyledButton>;
}

Button.defaultProps = {
  appearance: BUTTON_APPEARANCE.default,
  size: SIZE_TYPE.middle
};

export default Button;
