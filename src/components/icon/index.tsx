import React from "react";
import styled, { keyframes, css } from "styled-components";
import { icons } from "../shared/icons";
import { rotate } from "../shared/animation";

const Svg = styled.svg<IconProps>`
  display: ${props => (props.block ? "block" : "inline-block")};
  vertical-align: middle;
  animation: ${props =>
    props.spinning
      ? css`
          ${rotate} 1.2s linear infinite
        `
      : "none"};
  shape-rendering: inherit;
  transform: translate3d(0, 0, 0);
`;

const Path = styled.path`
  fill: ${props => props.color};
`;

export interface IconProps {
  /** 图标名*/
  icon: keyof typeof icons;
  /** 是否块级元素 */
  block?: boolean;
  /** 颜色 */
  color?: string;
  /** 是否旋转 */
  spinning?: boolean;
  /** 自定义样式 */
  style?: object;
  /** 宽度 */
  width?: string;
  /** 高度 */
  height?: string;
}

export function Icon(props: IconProps) {
  const { block, icon, color, style, width, height } = props;
  console.log("icon", icon);
  return (
    <span style={{ ...style, verticalAlign: "top" }}>
      <Svg
        viewBox="0 0 1024 1024"
        width={width}
        height={height}
        block={block}
        {...props}
      >
        <Path d={icons[icon]} color={color} />
      </Svg>
    </span>
  );
}
Icon.defaultProps = {
  block: false,
  color: "currentColor",
  spinning: false,
  width: "1em",
  height: "1em",
  style: {}
};
