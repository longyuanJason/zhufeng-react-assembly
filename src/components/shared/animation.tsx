import { keyframes } from "styled-components";

export const easing = {
  rubber: "cubic-bezier(0.175, 0.885, 0.335, 1.05)"
};
export const rotate = keyframes`
from {
  transform: rotate(0deg);
}

25% {
  transform: rotate(90deg);
}

50% {
  transform: rotate(180deg);
}

75% {
  transform: rotate(270deg);
}

to {
  transform: rotate(360deg);
}`;
